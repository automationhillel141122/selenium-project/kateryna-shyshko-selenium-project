package org.shyshko.pages.main_menu;

import org.openqa.selenium.By;
import org.shyshko.driver.WebdriverHolder;

public class MainMenu {

    public void selectLogin() {
        selectMenuItem(MainMenuItem.LOGIN);
    }

    public void selectRegister() {
        selectMenuItem(MainMenuItem.REGISTER);
    }

    public void selectLogout() {
        selectMenuItem(MainMenuItem.LOGOUT);
    }

    public void selectWishList() {
        selectMenuItem(MainMenuItem.WISH_LIST);
    }

    public void selectShoppingCart() {
        selectMenuItem(MainMenuItem.SHOPPING_CART);
    }

    public void selectMyAccount() {
        selectMenuItem(MainMenuItem.ACCOUNT);
    }

    private void selectMenuItem(MainMenuItem menuItem) {
        WebdriverHolder.getInstance().getDriver()
                .findElement(By.cssSelector("a.ico-" + menuItem.value()))
                .click();
    }

}
