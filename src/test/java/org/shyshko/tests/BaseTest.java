package org.shyshko.tests;

import org.shyshko.driver.WebdriverHolder;
import org.shyshko.steps.NavigationSteps;
import org.shyshko.steps.ProductActionsSteps;
import org.shyshko.utils.PropertyReader;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;

public class BaseTest {

    protected NavigationSteps navigationSteps = new NavigationSteps();
    protected ProductActionsSteps productActionsSteps = new ProductActionsSteps();
    @BeforeClass
    public void beforeClassGlobal() {
        WebdriverHolder.getInstance().getDriver().get(PropertyReader.getProperty("base.url"));
    }

    @AfterSuite
    public void afterSuite() {
        WebdriverHolder.getInstance().getDriver().quit();
    }
}
