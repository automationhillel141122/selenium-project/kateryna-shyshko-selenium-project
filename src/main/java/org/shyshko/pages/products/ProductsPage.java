package org.shyshko.pages.products;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.shyshko.driver.WebdriverHolder;
import org.shyshko.pages.BasePage;

import java.util.ArrayList;
import java.util.List;

public class ProductsPage extends BasePage {

    String itemBox = "//div[@class='item-box']//div[@class='details']//a[text()='%s']";

    public ProductsPage() {
        super();
    }

    public int getItemsCount() {
        return WebdriverHolder.getInstance().getDriver()
                .findElements(By.cssSelector(".item-box"))
                .size();
    }

    public ProductsPage addProductToCart(String productName) {
        WebDriver driver = WebdriverHolder.getInstance().getDriver();
        WebElement element = driver.findElement(By.xpath(itemBox.formatted(productName)));
        WebElement productItem = element.findElement(By.xpath("./../.."));
        productItem.findElement(By.cssSelector(".buttons .product-box-add-to-cart-button")).click();
        return new ProductsPage();
    }


    public List<ProductModel> getProducts() {
        List<ProductModel> productModels = new ArrayList<>();
        List<WebElement> elements = WebdriverHolder.getInstance().getDriver().findElements(By.xpath("//div[@class='item-box']//div[@class='details']"));
        for (WebElement element : elements) {
            String productName = element.findElement(By.xpath(".//h2/a")).getText();
            Long price = Long.parseLong(element.findElement(By.cssSelector(".price.actual-price"))
                    .getText()
                    .substring(1)
                    .replaceAll("\\.", "")
                    .replaceAll(",", ""));
            productModels.add(new ProductModel(productName, price));
        }
        return productModels;
    }


}
