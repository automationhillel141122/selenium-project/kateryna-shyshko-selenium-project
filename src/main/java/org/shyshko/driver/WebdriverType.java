package org.shyshko.driver;

public enum WebdriverType {
    CHROME,
    FIREFOX,
    SAFARI
}
