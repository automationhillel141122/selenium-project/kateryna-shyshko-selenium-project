package org.shyshko.pages.category_menu;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.shyshko.driver.WebdriverHolder;
import org.shyshko.pages.BasePage;
import org.shyshko.pages.products.ProductsPage;

public class CategoryMenu {
    public BasePage selectMainCategory(String mainCategory) {
        WebDriver driver = WebdriverHolder.getInstance().getDriver();
        driver.findElement(By.xpath("//ul[@class='top-menu notmobile']/li/a[starts-with(.,'%s')]".formatted(mainCategory)))
                .click();
        return new BasePage();
    }

    public ProductsPage selectSubCategory(String mainCategory, String subCategory) {
        WebDriver driver = WebdriverHolder.getInstance().getDriver();
        WebElement mainCategoryElement =
                driver.findElement(By.xpath("//ul[@class='top-menu notmobile']/li/a[starts-with(.,'%s')]".formatted(mainCategory)));
        Actions actions = new Actions(driver);
        actions
                .moveToElement(mainCategoryElement)
                .build()
                .perform();
        mainCategoryElement
                .findElement(By.xpath("./.."))
                .findElement(By.xpath("//ul[@class='sublist first-level']/li/a[starts-with(.,'%s')]".formatted(subCategory)))
                .click();
        return new ProductsPage();
    }
}
